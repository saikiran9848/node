const {dynamoDB} = require('../connections/awsConnection');
const logger = require('../helpers/logger');
const config = require("../config/config");
const params = {
    TableName: config.favorites.table,
    AttributeDefinitions: [{
        AttributeName: 'Id', //primary key
        AttributeType: 'S'
    },
        {
            AttributeName: 'UserId', // index key / sort key
            AttributeType: 'S'
        }
    ],
    KeySchema: [{
        AttributeName: 'Id',
        KeyType: 'HASH'
    },
        {
            AttributeName: 'UserId',
            KeyType: 'RANGE'
        }
    ],
    GlobalSecondaryIndexes: [ // optional (list of GlobalSecondaryIndex)
        {
            IndexName: config.favorites.index,
            KeySchema: [{ // Required HASH type attribute
                AttributeName: 'UserId',
                KeyType: 'HASH',
            }],
            Projection: { // attributes to project into the index
                ProjectionType: 'ALL' // (ALL | KEYS_ONLY | INCLUDE)
            },
            ProvisionedThroughput: { // throughput to provision to the index
                ReadCapacityUnits: 10,
                WriteCapacityUnits: 10,
            },
        },
 
    ],
 
    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
    },
};
 
dynamoDB.listTables({}, function(err, data) {
    let tables = null;
    if(data){
      tables= data.TableNames;
    }
    if(!data || tables.indexOf(params.TableName) == -1){
        dynamoDB.createTable(params, function (err, data) {
            if (err) {
                logger.error("Unable to create table. Error JSON:"+ JSON.stringify(err, null, 2));
            } else {
                logger.info("Created table. Table description JSON:"+ JSON.stringify(data, null, 2));
            }
        });
    }
});