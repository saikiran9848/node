const redisCluster = require("../connections/redisConnection");
const awsObj = require("../connections/awsConnection");
const dynamoDB = require('../connections/dynamoConnection');
const logger = require('../helpers/logger');

exports.healthCheck = async (req, res, next) => {
    try {

        //Check Redis
        let adRedisHealth = {};
        let health = {};
        adRedisHealth["host"] = process.env.REDIS_HOST;
        adRedisHealth["status"] = redisCluster.status == "ready" ? "Working" : "Down";
        logger.info(JSON.stringify(adRedisHealth));
        health["redis"] = {adRedisHealth};

        //Check Dynamodb
        adDynamoHealth = {}
        adDynamoHealth["region"] = process.env.AWS_REGION;
        adDynamoHealth["status"] = "Down";
        const params = {
            TableName : 'userapi.helthcheck',
            Key: {
              id: 1
            }
          }
          result = await dynamoDB.getAllDynamo(params);
          if (result.Items.length) {
            adDynamoHealth["status"] = "ready";
          }
          health["dynamo"]={adDynamoHealth};
          res.status(200).send(health);

    } catch (err) {
        logger.error(JSON.stringify(err));
        res.status(404).send(err);
    }
};


