const express = require("express");
const router = express.Router();
const watchlistController = require("../controllers/watchlistController");
const { verifyWatchlist } = require("../services/watchlist/watchlistValidation");

// get All Watchlist
router.get("/", watchlistController.getWatchlist);

// add Watchlist
router.post("/", verifyWatchlist, watchlistController.addWatchlist);

// update Watchlist
router.put("/", verifyWatchlist, watchlistController.updateWatchlist);

// update Watchlist
router.delete("/", watchlistController.deleteWatchlist);

// export router.
module.exports = router;