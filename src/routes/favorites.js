const express = require('express');
const router = express.Router();
const favoritesController = require('../controllers/favoritesController');
const { verifyFavorites } = require('../services/favorites/favoritesValidation');

// get All favorites
router.get('/', favoritesController.getFavorites);

// add favorites
router.post('/', verifyFavorites, favoritesController.addFavorites);

// update favorites
router.put('/', verifyFavorites, favoritesController.updateFavorites);

// update favorites
router.delete('/', favoritesController.deleteFavorites);

//global favorites
module.exports = router;