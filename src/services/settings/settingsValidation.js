const checkobject = require('./../../helpers/requestHelper');
const logger = require('../../helpers/logger')

exports.addSettings = (req, res, next) => {
  let body = req.body
  if(checkobject.isEmpty(body) || !body.hasOwnProperty("key")){
    const error = new Error('');
    error.message = 'No item in request'
    error.statusCode = 3;
    error.status = 400;
    logger.error(JSON.stringify(error));
    throw error;
    // logger.error(JSON.stringify(error));
  }else{
    next();
  }
}

exports.updateSettingsData = (req, res, next) => {
  let body = req.body;
  if(checkobject.isEmpty(body) || !body.hasOwnProperty("key")){
    const error = new Error('');
    error.message = 'No item in request'
    error.statusCode = 3;
    error.status = 400;
    logger.error(JSON.stringify(error));
    throw error;
  }else{
    next();
  }
}

exports.deleteSettingsData = (req, res, next) => {
  let body = req.body;
  if(checkobject.isEmpty(body)){
    const error = new Error('');
    error.message = 'No item in request'
    error.statusCode = 3;
    error.status = 400;
    logger.error(JSON.stringify(error));
    throw error;
  }else{
    next();
  }
}