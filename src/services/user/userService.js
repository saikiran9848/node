const bcryptService = require('../../helpers/bcrypt')
const { v4: uuidv4 } = require('uuid');
const dynamoDB = require('../../connections/dynamoConnection');
const otpService = require('../others/otpService');
const verifyUser = require("./verifyUser");
const config = require('../../config/config');
const _ = require("lodash");
var moment = require( 'moment' );
const deleted = 99;
const usertableName = config.users.table;
const userindexName = config.users.index;
const otptableName = config.onetimepassword.table;
const useremailindexName = config.users.email_index;
const UUID = require('uuid');
const logger = require('../../helpers/logger')
const userProfileUpdateHistoryTableName = config.userProfileUpdateHistory.table;
const tableName = 'Users';

module.exports = {


getUser : async (req, res) => {
    let user = await getRecord(req);

    return  user ? user.Item : "";
},


//updateUserV2
updateUserV2 : async (req, res) => {

},

  getUser : async (req, res) => {
    let userId = req.headers.userid;
    const params = {
        TableName: usertableName,
        Key: {
            "Id": userId
        }
    };
    return await dynamoDB.get(params);
  },
  getUserWithToken: async(inputUserId) =>{
    const params = {
        TableName: usertableName,
        Key: {
            "Id": inputUserId
        }
    };
    return (await dynamoDB.get(params)).Item;
  },
  setLastLogin : async( user) =>{
    try{
      var currentTime = new Date();
      const updateuserLogin = {
        TableName: usertableName,
        Key: {
               Id:  user.Id,
             },
        UpdateExpression: 'SET #LastLogin  = :LastLogin ',
        ConditionExpression: 'Id = :Id',
        ExpressionAttributeNames: { '#LastLogin': 'LastLogin' },
        ExpressionAttributeValues: {
             ':LastLogin': currentTime.valueOf(),
             ':Id' : user.Id
         },
              ReturnValues: "UPDATED_NEW",
          };
        return await dynamoDB.updateDynamo(updateuserLogin)
        } 
        catch (error) {
          logger.error(JSON.stringify(error))
      }
    },
 

  confirmMobile : async (req,res) => {
    let otpvalidation = await otpService.validateCode(req);

    if (otpvalidation.Items.length > 0) {
        var currentTime = new Date();

        if(otpvalidation["Items"][0].Expires >=  currentTime.valueOf()){

            let userValidation = await verifyUserService.validateUser(req);

            const updateParams = {
                TableName: usertableName,
                Key: {
                    Id: userValidation["Items"][0].Id,
                },
                UpdateExpression: 'SET #MobileConfirmationKey = :MobileConfirmationKey , #IsMobileConfirmed = :IsMobileConfirmed ,#Mobile = :Mobile',
                ConditionExpression: 'Id = :Id',
                ExpressionAttributeNames: { '#MobileConfirmationKey': 'MobileConfirmationKey' , '#IsMobileConfirmed':'IsMobileConfirmed' , '#Mobile': 'Mobile'},
                ExpressionAttributeValues: {
                    ':IsMobileConfirmed': true,
                    ':MobileConfirmationKey': req.body.code,
                    ':Mobile':userValidation["Items"][0].Mobile,
                    ':Id': req.headers.userid
                },
                ReturnValues: "UPDATED_NEW",
            };

            return await dynamoDB.updateDynamo(updateParams);
        }
        else{
            let error = new Error();

            error = { message: "Confirmation code expired", statusCode: 1, status: 400 }
            throw error;
        }
    }
    else{
        let error = new Error();
        error = { message: "Confirmation code was not found", statusCode: 1, status: 400 }
        throw error;
        }
 },

 confirmemailv2 : async (req,res) => {
    let otpValidation = await otpService.validateCode(req);

      if (otpValidation.Items.length > 0) {

          var currentTime = new Date();
           
          if(otpvalidation.Items[0].expires >=  currentTime.valueOf()){
            
            let uservalidation = await verifyUserService.validateUser(req);
              
            if(uservalidation.Items[0].email == otpvalidation.Items[0].recipientaddress){

                const params = {
                  TableName: usertableName,
                  Key: {
                        Id: userValidation["Items"][0].Id,
                       },
                  UpdateExpression: 'SET #EmailConfirmationKey = :EmailConfirmationKey , #IsEmailConfirmed = :IsEmailConfirmed , EmailConfirmationExpiration = :EmailConfirmationExpiration ',
                  ConditionExpression: 'Id = :Id AND Email= :Email',
                  ExpressionAttributeNames: { '#EmailConfirmationKey': 'EmailConfirmationKey' , '#IsEmailConfirmed':'IsEmailConfirmed' },
                  ExpressionAttributeValues: {
                       ':IsEmailConfirmed': true,
                       ':EmailConfirmationKey': req.body.code,
                       ':Email':userValidation.Items[0].Email,
                       ':EmailConfirmationExpiration':otpValidation.Items[0].Expires,
                       ':Id' : req.headers.userid
                            
                   },
                        ReturnValues: "UPDATED_NEW",
                    };

                  result = await dynamoDB.updateDynamo(params);
                  return result;

            }else if(userValidation.Items[0].Mobile === otpValidation.Items[0].Recipientaddress){

               const params = {
                  TableName: usertableName,
                  Key: {
                    Id: userValidation["Items"][0].Id,
                  },
                  UpdateExpression: 'SET #MobileConfirmationKey = :MobileConfirmationKey , #IsMobileConfirmed = :IsMobileConfirmed , MobileConfirmationExpiration = :MobileConfirmationExpiration ',
                  ConditionExpression: 'Id = :Id AND Mobile = :Mobile',
                  ExpressionAttributeNames: { '#MobileConfirmationKey': 'MobileConfirmationKey' , '#IsMobileConfirmed':'IsMobileConfirmed'},
                  ExpressionAttributeValues: {
                      ':IsMobileConfirmed': true,
                      ':MobileConfirmationKey': req.body.code,
                      ':Mobile':userValidation.Items[0].Mobile,
                      ':MobileConfirmationExpiration':otpValidation.Items[0].Expires,
                      ':Id': req.headers.userid
                      
                  },
                  ReturnValues: "UPDATED_NEW",
              };

                result = await dynamoDB.updateDynamo(params);
                return result;

            }else{

              let error = new Error();
              error = { message: "Invalid User", statusCode: 1, status: 404 }
              throw error;

            }

        }else {

            let error = new Error();
            error = { message: "Confirmation code expired", statusCode: 1, status: 400 }
            throw error;
        }

      }else{

        let error = new Error();
        error = { message: "Confirmation code was not found", statusCode: 1, status: 400 }
        throw error;

     }
   },

  //  //CRUD operations user creation placeholder
  //  CreateUserV2: async(req,res) =>{
  // //  console.log(JSON.stringify(req.body));
  // //generate Password Hash
  //   const hashedPassword =  bcryptService.createPasswordHash(req.body.password);
  //   const params = {
  //     TableName: usertableName,
  //     Item: {
  //       Id: UUID.v4(),
  //       FirstName: req.body.first_name,
  //       LastName: req.body.last_name,
  //       Email: req.body.email,
  //       Mobile: req.body.mobile,
  //       PasswordHash: (await hashedPassword).toString(),
  //       Gender: req.body.gender,
  //       Birthday: req.body.birthday,
  //       MacAddress:req.body.mac_address,
  //       EmailConfirmationKey: req.body.emailConfirmationKey,
  //       MobileConfirmationKey: req.body.mobileConfirmationKey,
  //       IsEmailConfirmed: req.body.isEmailConfirmed,
  //       IsMobileConfirmed:req.body.isMobileConfirmed,
  //       EmailConfirmationExpiration: req.body.emailConfirmationExpiration,
  //       MobileConfirmationExpiration: req.body.mobileConfirmationExpiration,
  //       LastLogin:req.body.lastLogin,
  //       State: req.body.state
  //     },
  //   };
  //   return await dynamoDB.addDynamo(params);
  //  },
    validateUserWithEmail : async (inputEmail) =>{
     try {
         const getParams = {
             TableName: usertableName,
             IndexName: useremailindexName, 
             KeyConditionExpression: 'Email = :value', 
             ExpressionAttributeValues: { 
                 ':value': inputEmail
             },
             ScanIndexForward: true, 
             Limit: 10, 
             ConsistentRead: false, 
             Select: 'ALL_ATTRIBUTES', 
             ReturnConsumedCapacity: 'NONE', 
        };
          return (await dynamoDB.getDynamo(getParams)).Items[0];
  
        } catch (error) {
          logger.error(JSON.stringify(error));
        }
     },
     validateUser : async (req) => {

        try {
        const getParams = {
            TableName: usertableName,
            KeyConditionExpression: 'id = :id',
            ExpressionAttributeValues: {
                ':id':req.headers.userid
           }
       };
            return await dynamoDB.getDynamo(getParams);
  
          } catch (error) {
            logger.error(JSON.stringify(error))
       }  
   },

    updatePassword : async(userId,newPwdHash) => {
        const params = {
            TableName: usertableName,
            Key: {
                Id: userId,
            },
            UpdateExpression: 'SET #PasswordHash = :PasswordHash',
            ConditionExpression: 'Id = :Id',
            ExpressionAttributeNames: { '#PasswordHash': 'PasswordHash'},
            ExpressionAttributeValues: {
                ':PasswordHash': newPwdHash,
                ':Id' : userId
            },
                ReturnValues: "UPDATED_NEW",
        }
        let result = await dynamoDB.updateDynamo(params);
        return result;
    },
    
    insertUserProfileUpdateHistory : async(itemData) => {
        const params = {
            TableName: userProfileUpdateHistoryTableName,
            Item: {
                Id: uuidv4(),
                UserId: itemData.UserId,
                EmailId: itemData.Email,
                MobileNumber: itemData.Mobile,
                IpAddress: itemData.ipAddress,
                CountryCode: itemData.countryCode,
                RequestPayload : itemData.RequestPayload,
                DateCreated: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')+'.'+new Date().getUTCMilliseconds(),
                PasswordUpdated: itemData.PasswordUpdated
            },
        };
        result = await dynamoDB.addDynamo(params);
        return result;
    },
    

	getUserFromId : async(userId) => {
		const params = {
			TableName: usertableName,
			Key: {
				"Id": userId
			}
		};
		return await dynamoDB.get(params);
	},

}


async function getRecord(req) {
    let userId = req.headers.userid;
    const params = {
        TableName: usertableName,
        Key: { "Id": userId }
    };
   let userDetails = await dynamoDB.get(params);

    if (_.isEmpty(userDetails) || userDetails.State == deleted) {
        let error = new Error();
        error = { message: "The user could not be found.", statusCode: 2, status: 422 };
        throw error;
    }
    return userDetails;
}
exports.deleteUser = async (req, res) => {

    let data = await isItemExists(req);
    var localDate = new Date();
    //One to show conversion
    let localDateUTC = moment.utc( localDate ).format('YYYYMMDDhhmmss');
    let deletedTimestamp = `_deleted_${localDateUTC}`;

    if (data.Items.length === 0) {
        let error = new Error();
        error = { message: "Item couldn't be found", statusCode: 2, status: 404 }
        throw error;
    }

    const params = {
        TableName: tableName,
        Key: {
            Id: data["Items"][0].Id,
        },
        UpdateExpression: "SET Email = :Email, Mobile = :Mobile, #St =:userState , FacebookUserId = :FacebookUserId , GoogleUserId = :GoogleUserId , TwitterUserId = :TwitterUserId,  AmazonUserId = :AmazonUserId  , PasswordHash = :PasswordHash ",
        ExpressionAttributeValues: {
            ":Email": (data["Items"][0].Email) ? `${data["Items"][0].Email}${deletedTimestamp}` :data["Items"][0].Email,
            ":Mobile": (data["Items"][0].Mobile) ? `${data["Items"][0].Mobile}${deletedTimestamp}` :data["Items"][0].Mobile,
            ":userState": "deleted",
            ":FacebookUserId": "",
            ":GoogleUserId": "",
            ":TwitterUserId": "",
            ":AmazonUserId": "",
            ":PasswordHash": ""
        },
        ExpressionAttributeNames: {
            "#St": "State"

        },
        ReturnValues: "UPDATED_NEW",
    };
    result = await dynamoDB.updateDynamo(params);

    // raise event as by the C# code. Need more detail on this event.

    return result;


}



isItemExists = async (req) => {
    try {
        const getParams = {
            TableName: tableName,
            KeyConditionExpression: "Id = :Id",
            ExpressionAttributeValues: {
                ":Id": req.headers.userid
            }
        };

        data = await dynamoDB.getDynamo(getParams);
        //console.log(data);
        return data;
    } catch (error) {
        console.error(error);
    }

}
