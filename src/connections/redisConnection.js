const Redis = require("ioredis");
const util = require('../middlewares/utility');

const client = new Redis.Cluster(
	[
		{
			port: process.env.REDIS_PORT,
			host: process.env.REDIS_HOST,
		},
	]);

client.on("error", function (err) {
	// util.log("Failed to connect redis",'error',err)
});
client.on("connect", function (Result) {
	util.log("connected to redis successfully")
});

module.exports = client;
